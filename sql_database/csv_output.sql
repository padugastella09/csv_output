-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 08, 2020 at 08:06 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `csv_output`
--

-- --------------------------------------------------------

--
-- Table structure for table `registrants`
--

DROP TABLE IF EXISTS `registrants`;
CREATE TABLE IF NOT EXISTS `registrants` (
  `col_ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skills` varchar(255) NOT NULL,
  PRIMARY KEY (`col_ID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `registrants`
--

INSERT INTO `registrants` (`col_ID`, `name`, `birthdate`, `email`, `phone`, `skills`) VALUES
(1, 'Carlo Sample', '1995-03-07', 'samplecarlo@sample.com', '09062230202', 'PHP, MySQL, HTML, CSS, JavaScript'),
(2, 'Stella Paduga', '1995-11-15', 'padugastella@gmail.com', '09126513550', 'HTML, CSS, JavaScript, MySQL'),
(3, 'Merlon Veloso', '2000-10-09', 'merlon@email.com', '322-4490', 'HTML, CSS, JavaScript'),
(4, 'asdf', '2002-04-09', 'asdf', '09177732100', 'asdf'),
(5, 'Maria Makiling', '1987-08-03', 'maria@makiling.com', '2352-235-252-1', 'Microsoft Office'),
(6, 'Juan Luna', '1917-05-04', 'juan@gmail.com', '23958327', 'Strategy'),
(7, 'qwre', '1915-07-12', 'qwre', '092285245217', 'qwre');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
