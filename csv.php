<?php
$connect = mysqli_connect("localhost", "root", "", "csv_output");
$query = "SELECT * FROM registrants";
$result = mysqli_query($connect, $query);
if (isset($_POST["export_data"])) {
   if (mysqli_num_rows($result) > 0) {
      $num_column = mysqli_num_fields($result);
      $csv_header = '';
      for ($i = 0; $i < $num_column; $i++) {
         $csv_header .= '"' . mysqli_fetch_field_direct($result, $i)->name . '",';
      }
      $csv_header .= "\n";

      $csv_row = '';

      while ($row = mysqli_fetch_row($result)) {
         for ($i = 0; $i < $num_column; $i++) {
            $csv_row .= '"' . $row[$i] . '",';
         }
         $csv_row .= "\n";
      }
      header("Content-Type: text/csv");
      header("Content-Disposition: attachment; filename=file.csv");
      echo $csv_header . $csv_row;
   }
}
