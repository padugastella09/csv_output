<?php
$connect = mysqli_connect("localhost", "root", "", "csv_output");
$sql = "SELECT * FROM registrants";
$result = mysqli_query($connect, $sql);
?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Csv-Output</title>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<script src="jquery-3.2.1.min.js"></script>

<body>
   <div class="container-fluid mt-5">
      <div class="col-md-6 offset-md-3">
         <h4 class="text-center">CSV OUTPUT</h4>
         <table class="table mt-5">
            <thead>
               <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">Birthdate</th>
                  <th scope="col">Email</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Skills</th>
               </tr>
            </thead>
            <tbody>
               <?php
               if ($result->num_rows > 0) {
                  while ($row = mysqli_fetch_array($result)) {
                     echo
                        '<tr>  
                           <td>' . $row["col_ID"] . '</td>  
                           <td>' . $row["name"] . '</td>  
                           <td>' . $row["birthdate"] . '</td>  
                           <td>' . $row["email"] . '</td>  
                           <td>' . $row["phone"] . '</td>  
                           <td>' . $row["skills"] . '</td>  
                        </tr> ';
                  }
               } else {
                  echo
                     '<tr>
                     <td colspan="5">
                        No Data found...
                     </td>
                  </tr>';
               }
               ?>
            </tbody>
         </table>
         <form action="csv.php" method="post" enctype='multipart/form-data'>
            <div class="mt-3">
               <div class="mr-auto p-2 bd-highlight">
                  <input type="submit" name="export_data" class="btn btn-primary text-center" value="Export Data" />
               </div>
            </div>
         </form>
      </div>
   </div>
</body>

</html>